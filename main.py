from material_app import MaterialApp
from kivy.lang import Builder
from kivy.utils import get_color_from_hex
from kivy.animation import Animation
from kivy.storage.jsonstore import JsonStore
from kivymd.theming import ThemeManager
from kivymd.uix.list import OneLineAvatarIconListItem, MDList
from kivymd.uix.snackbar import Snackbar


store = JsonStore('data.json')

KV = """
BoxLayout   
    orientation: "vertical"
    spacing: dp(10)
    
    MDToolbar
        title: "Lista de Tarefas"
        anchor_title: "center"
        md_bg_color: app.theme_cls.primary_color

    BoxLayout
        orientation: "vertical"
        padding: dp(10), dp(0)

        BoxLayout
            size_hint_y: None
            height: dp(80)
            spacing: dp(10)

            MDTextField
                id: field_tarefa
                hint_text: "Nova Tarefa"
                font_size: dp(24)
                                
            MDRaisedButton
                text: "ADD"
                pos_hint: {"center_y": .5}
                on_press: container_list.add_item(field_tarefa)

        ScrollView
            do_scroll_x: False
            MyMDList
                id: container_list

<MDItem>
    icon: 'alert-octagon' if self.checked == 'normal' else 'check-bold'
    canvas_width: 0
    canvas_color: 1,0,0,1
    checked: icon_right.state
    
    canvas.before:
        Color:
            rgba: root.canvas_color
        Rectangle:
            size: root.canvas_width, self.height
            pos: self.pos

    IconLeftSampleWidget
        icon: root.icon
        theme_text_color: "Custom"
        text_color: app.theme_cls.primary_color
        
    IconRightSampleWidget
        id: icon_right

<IconRightSampleWidget@IRightBodyTouch+MDCheckbox>
<IconLeftSampleWidget@ILeftBodyTouch+MDIconButton>
"""

def toast(text):
    from kivymd.toast.kivytoast import toast
    toast(text)


class MyMDList(MDList):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        
        for index, tarefa in enumerate(store.keys()):
            state = store.get(tarefa)['completa'] == 'normal'
            one_list_item = MDItem(text=tarefa)
            one_list_item.ids['icon_right'].state = 'normal' if state else 'down'
            self.add_widget(one_list_item, index=index) # insere antes do ultimo filho'
    
    def add_item(self, tarefa):
        one_list_item = MDItem(text=tarefa.text)
        # insere antes do ultimo filho
        self.add_widget(one_list_item, index=len(self.children)) 

        # adiciona a tarefa no banco de dados
        store.put(tarefa.text, completa='normal')

        # limpa o campo tarefa
        tarefa.text = ''

    def animation_complete(self):
        prin(self)

    def remove_item(self, tarefa):
        index = self.children.index(tarefa)
        state = tarefa.checked
        self.remove_widget(tarefa)
        self.show_snackbar(index, tarefa, state)
        if store.exists(tarefa):
            store.delete(tarefa)

    def show_snackbar(self, index, tarefa, state):
        
        self.desfazer = False
        def callback(instance):
            self.desfazer = True
            one_list_item = MDItem(text=tarefa.text)
            one_list_item.ids['icon_right'].state = state
            self.add_widget(one_list_item, index=index)
            store.put(tarefa.text, completa=state)
        from kivy.factory import Factory
        snackbar = Snackbar(
            text=f"Tarefa {tarefa.text} removida!",
            button_text="Desfazer!",
            button_callback=callback,
        )
        button = snackbar.ids.box.children[0]
        button.theme_text_color = 'Custom'
        button.text_color = get_color_from_hex('ffffff')
        snackbar.show()

    def snackbar_completed(self, item):
        tarefa = item.text
        if store.exists(tarefa):
            store.delete(tarefa)

class MDItem(OneLineAvatarIconListItem):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.bind(checked=self.on_checked)

    def __repr__(self):
        return f'MDitem({self.text})'

    def on_checked(self, instance, state):
        # atualizar status da tarefa banco de dados
        tarefa = self.text
        store.put(tarefa, completa=state)

    def on_touch_up(self, touch):
        if touch.grab_current is self:
            if self.canvas_width > self.width * .75:
                # remove item da lista
                tarefa = self.text
                self.parent.remove_item(self)                
            else:
                # volta a lista para o estado inicial
                pos_x = 0
                pos_y = self.ids._text_container.pos[1]
                icon = 'alert-octagon' if self.checked == 'normal' else 'check-bold'

                anim = Animation(pos=(pos_x, pos_y), duration=.2)
                anim2 = Animation(canvas_width=0, duration=.2)

                anim.start(self.ids._text_container)
                anim2.start(self)

                self.icon = icon

            touch.ungrab(self)
            return True

    def on_touch_move(self, touch):
        if self.collide_point(touch.ox, touch.oy):
            self.canvas_width = touch.x - touch.ox
            self.ids._text_container.pos = [ self.canvas_width, self.ids._text_container.pos[1]]          
            if self.canvas_width < 0:
                # implementar efeito: barra vermelha surge da esquerda pra direita
                pass

    def on_touch_down(self, touch):
        if self.collide_point(*touch.pos):
            if not self.ids._right_container.collide_point(*touch.pos):
                self.icon = 'trash-can'
                touch.grab(self)
            else:
                if self.ids.icon_right.state == 'normal':
                    self.ids.icon_right.state = 'down'
                else:
                    self.ids.icon_right.state = 'normal'
            return True

    def start_ripple(self):
        """sobreescreve o efeito ripple"""
        pass


class ListaDeTarefas(MaterialApp):
    title = "Lista de Tarefas"
    theme_cls = ThemeManager()

    def build(self):
        return Builder.load_string(KV)
 

if __name__ == '__main__':
    ListaDeTarefas().run()