from kivy.app import App
from kivy.utils import get_hex_from_color
from android.runnable import run_on_ui_thread
from jnius import autoclass


Color = autoclass("android.graphics.Color")
WindowManager = autoclass('android.view.WindowManager$LayoutParams')
activity = autoclass('org.kivy.android.PythonActivity').mActivity


class MaterialApp(App):
    
    statusbar_color = True

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        if self.statusbar_color:
            color = get_hex_from_color(self.theme_cls.primary_color[:3])
            self.set_statusbar_color(color)

    @run_on_ui_thread
    def set_statusbar_color(self, color):
        window = activity.getWindow()
        window.clearFlags(WindowManager.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.setStatusBarColor(Color.parseColor(color))