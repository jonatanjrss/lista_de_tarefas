# Vídeo de demonstração do aplicativo
Link: https://youtu.be/h0qkw_rOH4U


# Importante
Se for executar esse código no linux ou windows, altere as linhas no arquivo ``main.py``:

``from material_app import MaterialApp``

``class ListaDeTarefas(MaterialApp):``

para:

``from kivy.app import App``

``class ListaDeTarefas(App):``
